public class Movie {
    private final int movieLength;
    private final String movieName;
    private String description;
    private final int ageRating;
    //price int Rappen
    private int price;

    public Movie(int movieLength, String movieName, String description, int ageRating, int price) {
        this.movieLength = movieLength;
        this.movieName = movieName;
        this.description = description;
        this.ageRating = ageRating;
        this.price = price;
    }

    public int getMovieLength() {
        return movieLength;
    }

    public String getMovieName() {
        return movieName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAgeRating() {
        return ageRating;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
