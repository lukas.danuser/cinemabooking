import java.util.Date;

public class Customer {
    private final String name;
    private final Date birthDate;

    Customer(String name, Date birthDate){
        this.name = name;
        this.birthDate = birthDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getName() {
        return name;
    }
}
