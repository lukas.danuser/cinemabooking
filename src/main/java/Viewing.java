public class Viewing {
    private final Movie movie;
    private final Room room;
    private final String time;
    private final int length;
    private int availableSeats;
    private boolean isArchived = false;
    private int breakeLength;

    public Viewing(Movie movie, Room room, String time, int breakLength) {
        this.movie = movie;
        this.room = room;
        this.time = time;
        availableSeats = room.getSeatAmount();
        length = movie.getMovieLength() + breakLength;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void reduceAvailableSeats() {
        availableSeats--;
    }

    public int getLength() {
        return length;
    }

    public int getBreakeLength() {
        return breakeLength;
    }

    public String getTime() {
        return time;
    }

    public Room getRoom() {
        return room;
    }

    public Movie getMovie() {
        return movie;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }
}
