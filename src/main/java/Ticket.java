public class Ticket {
    private final int seat;
    private final int row;
    private final int roomID;
    private final Viewing viewing;
    private final int price;
    private final Customer customer;

    public Ticket(int seat, int row, int roomID, Viewing viewing, Customer customer) {
        this.seat = seat;
        this.row = row;
        this.roomID = roomID;
        this.viewing = viewing;
        price = viewing.getMovie().getPrice();
        this.customer = customer;
    }

    //    public Ticket(int seat, int row, int roomID, Viewing viewing, Customer customer, int price) {
//        this.seat = seat;
//        this.row = row;
//        this.roomID = roomID;
//        this.viewing = viewing;
//        this.price = price;
//        this.customer = customer;
//    }
    public int getSeat() {
        return seat;
    }

    public int getRow() {
        return row;
    }

    public int getRoomID() {
        return roomID;
    }

    public Viewing getViewing() {
        return viewing;
    }

    public int getPrice() {
        return price;
    }

    public Customer getCustomer() {
        return customer;
    }

    public String getInfo() {
        String output = "";
        output += "Movie name: " + viewing.getMovie().getMovieName();
        output += "\nMovie length: " + viewing.getLength();
        output += "\nMovie description: " + viewing.getMovie().getDescription();
        output += "\nMovie rating: " + viewing.getMovie().getAgeRating();
        output += "\nTime: " + viewing.getTime();
        output += "\nSeat: " + seat;
        output += "\nrow: " + row;
        output += "\nRoom ID: " + roomID;
        output += "\nprice: " + price;
        return output;
    }
}
