import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    private final ArrayList<Room> rooms = new ArrayList<>();
    private final ArrayList<Customer> customers = new ArrayList<>();
    private final ArrayList<Ticket> tickets = new ArrayList<>();
    private final ArrayList<Viewing> viewings = new ArrayList<>();
    private final ArrayList<Movie> movies = new ArrayList<>();
    private boolean isAdmin = false;
    private Customer customer = null;

    public static void main(String[] args) {
        Main main = new Main();
        main.start();
    }

    private void start() {
        //do Stuff
        rooms.add(new Room(10, 10));
        try {
            customers.add(new Customer("defaultCustomer", new SimpleDateFormat("dd-MM-yyyy").parse("12-12-1212")));
            movies.add(new Movie(120, "testMovie", "DESCRIPTION", 16, 1500));
            viewings.add(new Viewing(movies.get(0), rooms.get(0), "2222-12-21T12:21:12", 15));
            tickets.add(new Ticket(15, 1, 0, viewings.get(0), customers.get(0)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        displayUI();
    }

    private boolean loadJSON() throws IOException {
        String[] jsonFiles = {"customers", "movies", "rooms", "tickets", "viewings"};
        boolean success = false;
        Gson gson = new Gson();
        rooms.clear();
        customers.clear();
        movies.clear();
        viewings.clear();
        tickets.clear();

        for (String filename : jsonFiles) {
            Path path = Paths.get("./saves/" + filename + ".json");
            Reader reader = Files.newBufferedReader(path);

            switch (filename) {
                case "customers": {
                    boolean done = false;
                    try {
                        List<Customer> customers = new Gson().fromJson(reader, new TypeToken<List<Customer>>() {
                        }.getType());
                        for (Customer customer : customers) {
                            done = createCustomer(customer.getName(), customer.getBirthDate());
                        }
                        reader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        break;
                    }
                    System.out.println("Customer " + done);
                    break;
                }
                case "movies": {
                    boolean done = false;
                    try {
                        List<Movie> movies = new Gson().fromJson(reader, new TypeToken<List<Movie>>() {
                        }.getType());
                        for (Movie movie : movies) {
                            done = createMovie(movie.getMovieName(), movie.getPrice(), movie.getMovieLength(), movie.getDescription(), movie.getAgeRating());
                        }
                        reader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        break;
                    }
                    System.out.println("Movie " + done);
                    break;
                }
                case "rooms": {
                    boolean done = false;
                    try {
                        List<Room> rooms = new Gson().fromJson(reader, new TypeToken<List<Room>>() {
                        }.getType());
                        for (Room room : rooms) {
                            done = createRoom(room.getRowsAmount(), room.getSeatsPerRow());
                        }
                        reader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        break;
                    }
                    System.out.println("Room " + done);
                    break;
                }
                case "tickets": {
                    boolean done = false;
                    try {
                        List<Ticket> tickets = new Gson().fromJson(reader, new TypeToken<List<Ticket>>() {
                        }.getType());
                        for (Ticket ticket : tickets) {
                            done = createTicket(ticket.getSeat(), ticket.getRow(), ticket.getRoomID(), ticket.getViewing(), ticket.getCustomer());
                        }
                        reader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        break;
                    }
                    System.out.println("Ticket " + done);
                    break;
                }
                case "viewings": {
                    boolean done = false;
                    try {
                        List<Viewing> viewings = new Gson().fromJson(reader, new TypeToken<List<Viewing>>() {
                        }.getType());
                        for (Viewing viewing : viewings) {
                            done = createViewing(viewing.getMovie(), viewing.getRoom(), viewing.getTime(), viewing.getBreakeLength());
                        }
                        reader.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        break;
                    }
                    System.out.println("Viewing " + done);
                    break;
                }
            }
        }
        return success;
    }

    private boolean saveJSON() throws IOException {
        String[] jsonFiles = {"customers", "movies", "rooms", "tickets", "viewings"};
        boolean success = false;
        Gson gson = new Gson();
        for (String file : jsonFiles) {
            Path path = Paths.get("./saves/" + file + ".json");
            Writer writer = Files.newBufferedWriter(path,
                    StandardCharsets.UTF_8, StandardOpenOption.CREATE);
            PrintWriter pw = new PrintWriter("./saves/" + file + ".json");
            pw.close();
            String json = gson.toJson(customers);
            switch (file) {
                case ("customers"): {
                    json = gson.toJson(customers);
                    break;
                }
                case ("movies"): {
                    json = gson.toJson(movies);
                    break;
                }
                case ("rooms"): {
                    json = gson.toJson(rooms);
                    break;
                }
                case ("tickets"): {
                    json = gson.toJson(tickets);
                    break;
                }
                case ("viewings"): {
                    json = gson.toJson(viewings);
                    break;
                }
            }
            writer.write(json);
            writer.flush();
        }
        return success;
    }

    private void displayUI() {
        boolean cont = true;
        Scanner scanner = new Scanner(System.in);
        while (cont) {
            //do stuff
            System.out.println("What do you want to do?");
            System.out.println("Your options are:");
            System.out.println("1: create new User account");
            System.out.println("2: log in as admin");
            System.out.println("3: buy ticket");
            System.out.println("4: display own tickets");
            System.out.println("5: create Movie");
            System.out.println("6: create Viewing");
            System.out.println("7: Log in");
            System.out.println("8: Save to JSON");
            System.out.println("9: Load from JSOn");
            System.out.println("15: exit Program");
            int choice = 0;
            try {
                choice = Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                System.out.println("You absolute donkey");
            }
            switch (choice) {
                case 1: {
                    System.out.println("Enter your full name:");
                    String name = scanner.nextLine();
                    System.out.println("Enter your birthdate in the format dd-MM-yyy");
                    String date = scanner.nextLine();
                    SimpleDateFormat sdt = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        Date result = sdt.parse(date);
                        boolean success = createCustomer(name, result);
                        if (success) {
                            System.out.println("created customer");
                        } else {
                            System.out.println("failed to create customer");
                        }
                    } catch (Exception e) {
                        System.out.println("wrong format");
                    }
                    break;
                }
                case 2: {
                    isAdmin = true;
                    System.out.println("you are now admin");
                    break;
                }
                case 3: {
                    for (Viewing v : viewings) {
                        if (v.isArchived() || v.getAvailableSeats() <= 0) {
                            continue;
                        }
                        System.out.printf("Index of the following Viewing: %d\n", viewings.indexOf(v));
                        System.out.printf("Movie name: %s\n", v.getMovie().getMovieName());
                        System.out.printf("Movie length: %d\n", v.getLength());
                        System.out.printf("Movie rating: %d\n", v.getMovie().getAgeRating());
                        System.out.printf("Movie price: %d\n", v.getMovie().getPrice());
                        System.out.printf("Time: %s\n", v.getTime());
                        System.out.printf("Available Seats: %d\n", v.getAvailableSeats());
                        System.out.println();

                    }
                    int index = Integer.parseInt(scanner.nextLine());
                    int a = viewings.get(index).getAvailableSeats();
                    int b = viewings.get(index).getRoom().getSeatsPerRow();
                    int c = viewings.get(index).getRoom().getSeatAmount();
                    int d = c - a;
                    int r = (int) Math.ceil((float) (d + 1) / (float) b) - 1;
                    int co = (d + 1) % b + 1;
                    int roomID = rooms.indexOf(viewings.get(index).getRoom());
                    //
                    tickets.add(new Ticket(co, r, roomID, viewings.get(index), customer));
                    break;
                }
                case 4: {
                    for (Ticket t : tickets) {
                        if (t.getCustomer() != customer) {
                            continue;
                        }
                        System.out.println(t.getInfo());
                    }
                    break;
                }
                case 5: {
                    if (!isAdmin) {
                        System.out.println("You are not admin");
                        break;
                    }
                    System.out.println("Enter the movie name");
                    String name = scanner.nextLine();
                    System.out.println("Enter the runtime of the movie in minutes");
                    int length = Integer.parseInt(scanner.nextLine());
                    System.out.println("Enter the description of the movie(only use 1 line)");
                    String desc = scanner.nextLine();
                    System.out.println("Enter the age rating of the movie");
                    int rating = Integer.parseInt(scanner.nextLine());
                    System.out.println("Enter the price of the movie in Rappen");
                    int rappen = Integer.parseInt(scanner.nextLine());
                    movies.add(new Movie(length, name, desc, rating, rappen));
                    break;
                }
                case 6: {
                    if (!isAdmin) {
                        break;
                    }
                    for (Movie m : movies) {
                        System.out.printf("Index: %d\n", movies.indexOf(m));
                        System.out.printf("Name: %s\n", m.getMovieName());
                        System.out.println();
                    }
                    try {
                        System.out.println("select the index of the movie");
                        int indexMovie = Integer.parseInt(scanner.nextLine());
                        System.out.println("Enter the date and time using the format: yyyy-mm-ddThh:mm:ss");
                        String time = scanner.nextLine();
                        System.out.println("THe following rooms are available at that time:");
                        for (int i = 0; i < rooms.size(); i++) {
                            if (rooms.get(i) != null) {
                                System.out.printf("room %d is available\n", i);
                            }
                        }
                        System.out.println("choose a room");
                        int indexRoom = Integer.parseInt(scanner.nextLine());
                        System.out.println("choose the length of the break in minutes (min. 0 minutes)");
                        int breakLength = Integer.parseInt(scanner.nextLine());
                        viewings.add(new Viewing(movies.get(indexMovie), rooms.get(indexRoom), time, breakLength));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 7: {
                    for (Customer c : customers) {
                        System.out.printf("Index: %d\n", customers.indexOf(c));
                        System.out.printf("Name: %s\n", c.getName());
                        System.out.println();

                    }
                    System.out.println("choose a customer");
                    try {
                        int index = Integer.parseInt(scanner.nextLine());
                        customer = customers.get(index);
                        System.out.println("success");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 8: {
                    try {
                        saveJSON();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 9: {
                    try {
                        loadJSON();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 15: {
                    cont = false;
                    break;
                }
            }


        }
    }

    private boolean createMovie(String title, int price, int length, String description, int ageRating) {
        try {
            Movie m = new Movie(length, title, description, ageRating, price);
            movies.add(m);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean createCustomer(String name, Date birthDate) {
        try {
            Customer c = new Customer(name, birthDate);
            customer = c;
            customers.add(c);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean createRoom(int rowsAmount, int seatsPerRow) {
        try {
            Room r = new Room(rowsAmount, seatsPerRow);
            rooms.add(r);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean createTicket(int seat, int row, int roomID, Viewing viewing, Customer customer) {
        try {
            Ticket t = new Ticket(seat, row, roomID, viewing, customer);
            tickets.add(t);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean createViewing(Movie movie, Room room, String time, int breakLength) {
        try {
            Viewing v = new Viewing(movie, room, time, breakLength);
            viewings.add(v);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean buyTicket(Viewing viewing) {
        return 1 == 2;
    }

    private String createSaveFile() {
        String text = "";
        return text;
    }
}
