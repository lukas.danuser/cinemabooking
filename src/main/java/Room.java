import java.util.ArrayList;

public class Room {
    private final int rowsAmount;
    private final int seatsPerRow;
    private final int seatAmount;

    public Room(int rowsAmount, int seatsPerRow) {
        this.rowsAmount = rowsAmount;
        this.seatsPerRow = seatsPerRow;
        seatAmount = rowsAmount*seatsPerRow;
    }

    public int getSeatAmount() {
        return seatAmount;
    }

    public int getSeatsPerRow() {
        return seatsPerRow;
    }

    public int getRowsAmount() {
        return rowsAmount;
    }
}
